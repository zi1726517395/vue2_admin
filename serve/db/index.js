// const mysql = require('mysql')
import mysql from 'mysql'

const client = mysql.createConnection({
  host: 'localhost', // 数据域名
  user: 'root',
  password: 'admin123',
  database: 'my_db_01',
  port: '3306'
})

// 封装数据库操作语句
export function sqlFn (sql, arr, callback) {
  client.query(sql, arr, function (error, result) {
    if (error) {
      console.log('数据库语句错误')
      return
    }
    callback(result)
  })
}
